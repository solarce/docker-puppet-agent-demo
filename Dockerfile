FROM tmtk75/puppet

MAINTAINER brandon@inatree.org

ADD puppet /root/puppet/

# get the puppet agent installed
#RUN apt-get update
#RUN apt-get install curl -y
#RUN curl https://apt.puppetlabs.com/puppetlabs-release-precise.deb -o /root/puppetlabs-release-precise.deb
#RUN dpkg -i /root/puppetlabs-release-precise.deb
#RUN apt-get update
#RUN apt-get install puppet -y

# apply site.pp
RUN puppet apply --verbose --modulepath /root/puppet/modules /root/puppet/site.pp
